﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(NetworkView))]
public class GameManager : MonoBehaviour {
	#region Variables
	public const string UnityGameType = "WaitsOverOfficial";

	public static GameManager instance { get; private set; }
	public static string[] availableMaps = { "test" };
	public static Dictionary<NetworkPlayer, Player> players { get { return instance.playerList; } }
	public static Round round { get { return instance.currentRound; } }

	public TitleScreen titleScreen;
	public GUI ingameGui;
	public NetworkView netView;
	public Dictionary<NetworkPlayer, Player> playerList = new Dictionary<NetworkPlayer, Player>();

	Round currentRound = null;
	#endregion

	GameManager() {
		if (instance != null) Destroy(this);
		instance = this;
	}

	#region Public methods

	/// <summary>
	/// Start hosting a new game on the currently loaded map
	/// </summary>
	public void HostGame() {
		// Start hosting
		Network.InitializeServer(16, 5029, !Network.HavePublicAddress());
		MasterServer.RegisterHost(UnityGameType, "Unnamed server");

		// Switch to in-game GUI
		titleScreen.gameObject.SetActive(false);
		ingameGui.gameObject.SetActive(true);

		playerList[Network.player] = new Player();

		if (PlayerPrefs.HasKey("Nickname")) {
			playerList[Network.player].nickname = PlayerPrefs.GetString("Nickname");
		};

		// Start match on current map
		BeginRound();
	}

	/// <summary>
	/// Join a currently running server
	/// </summary>
	/// <param name="serverData">Server data obtained from Master Server</param>
	public void JoinGame(HostData serverData) {
		Network.Connect(serverData);

		// Switch to in-game GUI
		titleScreen.gameObject.SetActive(false);
		ingameGui.gameObject.SetActive(true);
	}

	/// <summary>
	/// TEMPORARY function for beginning a new round
	/// </summary>
	public void BeginRound() {
		currentRound = new Round();
		currentRound.duration = 5 * 60;
		currentRound.start = Time.time;
		currentRound.type = Round.GameType.Deathmatch;
		currentRound.StartMatch();
	}

	/// <summary>
	/// Send an Event Message to all connected players
	/// </summary>
	/// <param name="text">Event to broadcast</param>
	public void BroadcastEvent(string text) {
		netView.RPC("AddEventRPC", RPCMode.All, text);
	}


	#endregion
	#region RPC

	[RPC]
	void PlayerJoined(NetworkPlayer player, string nickname, NetworkMessageInfo info) {
		playerList[player] = new Player() { nickname = nickname };

		// Restart round if we are the only one around
		if (currentRound.status == Round.RoundStatus.InProgress && playerList.Count == 2) {
			BroadcastEvent("Another player has joined, restarting..");
			currentRound.ForceEnd();
		} else {
			BroadcastEvent(nickname + " has joined");
		}

		// Send the new player all the other player's data
		foreach (KeyValuePair<NetworkPlayer, Player> playerpair in playerList) {
			netView.RPC("SetPlayerData", player, playerpair.Key, playerpair.Value.nickname, playerpair.Value.netId);
		}
	}

	[RPC]
	void AddEventRPC(string text) {
		ingameGui.AddEvent(text);
	}

	[RPC]
	void SetPlayerData(NetworkPlayer player, string nickname, NetworkViewID nid) {
		playerList[player] = new Player() {
			nickname = nickname,
			netId = nid
		};
	}

	[RPC]
	void ChangeMap(string mapName, string syncName) {
		if (Application.loadedLevelName != mapName) {
			Application.LoadLevel(mapName);
		}
		if (syncName != "") {
			NetworkSync.Sync(syncName);
		}
	}

	#endregion
	#region Unity Callbacks

	void Awake() {
		DontDestroyOnLoad(gameObject);
		DontDestroyOnLoad(titleScreen.gameObject);
		DontDestroyOnLoad(ingameGui.gameObject);
		netView.group = 4;
	}

	void Update() {
		if (Network.isServer && currentRound != null) {
			currentRound.Update();
		}
	}

	void OnConnectedToServer() {
		string nickname = PlayerPrefs.HasKey("Nickname") ? PlayerPrefs.GetString("Nickname") : "Anon";
		netView.RPC("PlayerJoined", RPCMode.Server, Network.player, nickname);
	}

	void OnPlayerConnected(NetworkPlayer player) {
		if (Network.isServer) {
			netView.RPC("ChangeMap", player, Application.loadedLevelName, "");
		}
	}

	void OnPlayerDisconnected(NetworkPlayer player) {
		if (playerList.ContainsKey(player)) {
			playerList.Remove(player);
			if (Network.isServer) {
				BroadcastEvent(playerList[player].nickname + " left");
			}
		}
	}

	#endregion
}
