﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(NetworkView))]
public class NetworkSync : MonoBehaviour {
	#region Data structures
	class SyncInfo {
		public Dictionary<NetworkPlayer, bool> syncedPlayers = null;
		public bool synced = false;
		public SyncedDelegate cb = null;

		public SyncInfo() {
			syncedPlayers = new Dictionary<NetworkPlayer, bool>();

			// Add all currently connected players
			foreach (NetworkPlayer player in Network.connections) {
				syncedPlayers.Add(player, false);
			}
		}
	}

	#endregion
	#region Variables

	public delegate void SyncedDelegate();
	NetworkView netView;

	static NetworkSync instance;
	static Dictionary<string, SyncInfo> syncDict = new Dictionary<string, SyncInfo>();

	#endregion
	#region Public Methods

	public static void Sync(string key) {
		instance.netView.RPC("SyncRPC", RPCMode.Server, key);
	}

	public static void CreateSync(string key) {
		if (!Network.isServer) {
			Debug.LogError("afterSync called in client code");
			return;
		}

		if (syncDict.ContainsKey(key)) {
			Debug.LogError("createSync: sync [" + key + "] already exists");
		} else {
			SyncInfo info = new SyncInfo();
			syncDict.Add(key, info);
		}
	}

	public static void StopSync(string key) {
		if (syncDict.ContainsKey(key)) {
			syncDict.Remove(key);
		} else {
			Debug.LogError("stopSync: sync [" + key + "] does not exist");
		}
	}

	public static void AfterSync(string key, SyncedDelegate cb) {
		if (!Network.isServer) {
			Debug.LogError("afterSync: called in client code");
			return;
		}

		if (!syncDict.ContainsKey(key)) {
			Debug.LogError("afterSync: sync [" + key + "] does not exist");
			return;
		}

		// Set callback
		SyncInfo info = syncDict[key];
		info.cb = cb;

		// Check for completion
		RunSynced();
	}

	#endregion
	#region Private Methods

	static void RunSynced() {
		Dictionary<string, SyncInfo> completedSyncDict = new Dictionary<string, SyncInfo>();

		// Check for completion
		foreach (KeyValuePair<string, SyncInfo> pair in syncDict) {
			SyncInfo info = pair.Value;

			// If all players are synced add to completed list
			if (info.cb != null && !info.syncedPlayers.Any(x => !x.Value)) {
				completedSyncDict.Add(pair.Key, info);
			}
		}

		foreach (KeyValuePair<string, SyncInfo> pair in completedSyncDict) {
			// Remove from list to check
			StopSync(pair.Key);

			// Call cb
			pair.Value.cb();
		}
	}

	#endregion
	#region RPC

	[RPC]
	public void SyncRPC(string key, NetworkMessageInfo msgInfo) {
		if (!Network.isServer) {
			Debug.LogError("syncRPC called in client code");
			return;
		}

		if (!syncDict.ContainsKey(key)) {
			Debug.LogWarning("syncRPC received on non-existent key [" + key + "] from " + msgInfo.sender);
			return;
		}

		// Update info
		SyncInfo info = syncDict[key];
		info.syncedPlayers[msgInfo.sender] = true;

		// Check for completion
		RunSynced();
	}

	#endregion
	#region Unity Callbacks

	void Awake() {
		instance = this;
		netView = GetComponent<NetworkView>();
		netView.group = 2;
		DontDestroyOnLoad(gameObject);
	}

	void OnPlayerDisconnected(NetworkPlayer player) {
		foreach (KeyValuePair<string, SyncInfo> pair in syncDict) {
			SyncInfo info = pair.Value;
			info.syncedPlayers.Remove(player);
		}
	}

	#endregion

}
