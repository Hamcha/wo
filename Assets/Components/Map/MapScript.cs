﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(NetworkView))]
public class MapScript : MonoBehaviour {
	public static MapScript currentInstance { get; private set; }
	public Transform[] spawnPoints;
	public Dictionary<NetworkPlayer, GameObject> players;
	public GameObject playerPrefab;
	public NetworkView netView;

	GameObject player = null;

	void Awake() {
		currentInstance = this;
		netView.group = 3;
		players = new Dictionary<NetworkPlayer, GameObject>();

		// Spawn player if in editor
		if (Application.isEditor && GameObject.Find("GameManager") == null) {
			SpawnOfflinePlayer();
		}
	}

	public void SpawnRandom(GameObject player) {
		int spawnIndex = UnityEngine.Random.Range(0, spawnPoints.Length - 1);
		player.transform.position = spawnPoints[spawnIndex].position;
		player.transform.rotation = spawnPoints[spawnIndex].rotation;
		player.SetActive(true);
	}

	[RPC]
	public void CreatePlayerRPC(NetworkMessageInfo info) {
		GameObject netPlayer = Network.Instantiate(playerPrefab, Vector3.zero, Quaternion.identity, 0) as GameObject;
		SpawnRandom(netPlayer);
		netView.RPC("SetPlayerObject", RPCMode.All, Network.player, netPlayer.GetComponent<NetworkView>().viewID);

		if (Network.isClient) {
			NetworkSync.Sync("CreatePlayer");
		}
	}

	public void SpawnOfflinePlayer() {
		if (player == null) {
			player = Instantiate(playerPrefab);
		}
		SpawnRandom(player);
	}

	public GameObject[] GetAllPlayers() {
		return players.ToList().Select((x) => x.Value).ToArray();
	}

	[RPC]
	public void SetPlayerObject(NetworkPlayer player, NetworkViewID id, NetworkMessageInfo info) {
		GameManager.players[player].netId = id;
		players[player] = NetworkView.Find(id).gameObject;
	}

	public void DestroyAllPlayer() {
		foreach (GameObject player in GetAllPlayers()) {
			Network.Destroy(player);
		}
	}
}
