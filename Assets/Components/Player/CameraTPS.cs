﻿using UnityEngine;

public class CameraTPS : MonoBehaviour {
	public Transform playerTransform, pivotTransform;
	public float sensitivity = 10;
	public float yThreshold = 10;

	Quaternion cameraRotation, pivotRotation, playerRotation;
	Vector2 rotation = Vector2.zero;

	void Start() {
		Cursor.lockState = CursorLockMode.Locked;
		pivotRotation = pivotTransform.localRotation;
		playerRotation = playerTransform.localRotation;
		cameraRotation = transform.localRotation;
	}

	void Update() {
		rotation.x += Input.GetAxis("Mouse X") * sensitivity;
		rotation.y += Input.GetAxis("Mouse Y") * sensitivity;
		rotation.x = ClampAngle(rotation.x, -360, 360);
		rotation.y = ClampAngle(rotation.y, -60, 40);

		Quaternion rotationX = Quaternion.AngleAxis(rotation.x, Vector3.up);
		Quaternion rotationYpivot = Quaternion.AngleAxis(-rotation.y, Vector3.right);
		Quaternion rotationYcamera =
			rotation.y < yThreshold
				? Quaternion.identity
				: Quaternion.AngleAxis(-rotation.y + yThreshold, Vector3.right);

		playerTransform.localRotation = playerRotation * rotationX;
		pivotTransform.localRotation = pivotRotation * rotationYpivot;
		transform.localRotation = cameraRotation * rotationYcamera;
	}

	float ClampAngle(float angle, float min, float max) {
		while (angle < -360) angle += 360;
		while (angle > 360) angle -= 360;
		return Mathf.Clamp(angle, min, max);
	}
}
