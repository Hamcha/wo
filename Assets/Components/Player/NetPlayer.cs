﻿using UnityEngine;
using System.Collections;

public class NetPlayer : MonoBehaviour {
	public PlayerMovement playerMovement;
	public NetworkView netView;
	public GameObject playerCamera, globalCamera;

	VectorInterpolator posInterpolator, rotInterpolator;
	Vector3 lastKnownPosition, lastKnownDirection;

	void OnNetworkInstantiate(NetworkMessageInfo info) {
		if (netView.isMine) {
			Destroy(this);
			return;
		}

		playerMovement.enabled = false;
		Destroy(playerCamera);
		posInterpolator = new VectorInterpolator();
		rotInterpolator = new VectorInterpolator();
	}

	void Update() {
		if (posInterpolator != null && !posInterpolator.stopped) {
			transform.position += posInterpolator.Value;
		}
		if (rotInterpolator != null && !rotInterpolator.stopped) {
			transform.forward += rotInterpolator.Value;
		}
	}

	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
		Vector3 pos = stream.isWriting ? transform.position : Vector3.zero;
		Vector3 rot = stream.isWriting ? transform.forward : Vector3.forward;

		stream.Serialize(ref pos);
		stream.Serialize(ref rot);

		if (stream.isReading) {
			if (lastKnownPosition == pos || !posInterpolator.Start(pos - transform.position)) {
				transform.position = pos;
			}

			if (lastKnownDirection == rot || !posInterpolator.Start(rot - transform.forward)) {
				transform.forward = rot;
			}

			lastKnownPosition = pos;
			lastKnownDirection = rot;
		}
	}
}