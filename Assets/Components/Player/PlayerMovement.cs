﻿using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovement : MonoBehaviour {
	public float speed = 5;
	public float jumpForce = 4;
	public float airControl = 0.9f;
	public float dashSpeed = 3f;
	public float jumpDelay = 0.3f;

	float currentJumpDelay = 0;
	bool jumpReleased = true;
	int jumpStage = 0;
	Vector3 gravity = Vector3.zero;
	CharacterController playerController;

	void Start() {
		playerController = GetComponent<CharacterController>();
	}

	void Update() {
		// Movement
		float front = Input.GetAxis("Vertical");
		float side = Input.GetAxis("Horizontal");
		Vector3 velocity = transform.forward * front + transform.right * side;
		if (velocity.magnitude > 1) {
			velocity.Normalize();
		}
		velocity *= speed;

		// Jumping
		if (currentJumpDelay > 0) currentJumpDelay -= Time.deltaTime;
		if (!playerController.isGrounded) {
			velocity *= airControl;
			if (gravity.y > Physics.gravity.y) {
				gravity.y += Physics.gravity.y * Time.deltaTime;
			}
		} else {
			gravity = Vector3.zero;
			jumpStage = 0;
		}

		if (Input.GetButton("Jump")) {
			// Dashing from air
			if (jumpReleased && jumpStage == 1 && !playerController.isGrounded) {
				currentJumpDelay = jumpDelay;
				jumpReleased = false;
				gravity += velocity.normalized * dashSpeed;
				jumpStage = 2;
			}
			// Jumping from ground
			if (jumpReleased && jumpStage == 0 && playerController.isGrounded && currentJumpDelay <= 0) {
				currentJumpDelay = jumpDelay;
				jumpReleased = false;
				gravity = Vector3.up * jumpForce;
				jumpStage = 1;
			}
		}

		// Avoid bunny hopping / dash spamming
		if (Input.GetButtonUp("Jump") && !jumpReleased) {
			jumpReleased = true;
		}

		velocity += gravity;
		playerController.Move(velocity * Time.deltaTime);
	}
}
