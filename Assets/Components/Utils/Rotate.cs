﻿using UnityEngine;

public class Rotate : MonoBehaviour {
	public Vector3 amount;

	void Update () {
		transform.Rotate(amount * Time.deltaTime);
	}
}
