﻿using UnityEngine;
using UnityEngine.UI;

public class ServerButton : MonoBehaviour {
	public HostData serverData;
	public Text serverNameTx, playerCountTx;

	public void Setup(HostData host) {
		serverData = host;
		GetComponent<Button>().onClick.AddListener(Clicked);
		serverNameTx.text = "<b>" + host.gameName + "</b> " + host.comment;
		playerCountTx.text = "<b>" + host.connectedPlayers + "</b> / " + host.playerLimit;
	}

	public void Clicked() {
		GameManager.instance.JoinGame(serverData);
	}
}
