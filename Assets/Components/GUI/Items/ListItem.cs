﻿using UnityEngine;
using UnityEngine.UI;

public class ListItem : MonoBehaviour {
	public Text textField;

	public void SetText(string text) {
		textField.text = text;
	}
}
