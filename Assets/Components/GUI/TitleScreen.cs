﻿using UnityEngine;
using UnityEngine.UI;

public class TitleScreen : MonoBehaviour {
	public GameObject serverItemPrefab;
	public GameObject serverList;
	public InputField nickBox;

	bool refreshing = false;

	void Start() {
		// Load random level beneath title screen
		int mapIndex = Random.Range(0, GameManager.availableMaps.Length - 1);
		Application.LoadLevel(GameManager.availableMaps[mapIndex]);

		// Request host list from Master Server
		RefreshList();

		if (PlayerPrefs.HasKey("Nickname")) {
			nickBox.text = PlayerPrefs.GetString("Nickname");
		}
	}

	void Update() {
		if (refreshing) {
			HostData[] hostData = MasterServer.PollHostList();
			if (hostData.Length > 0) {
				refreshing = false;
				foreach (HostData host in hostData) {
					AddHostToList(host);
				}
				MasterServer.ClearHostList();
			}
		}
	}

	void AddHostToList(HostData host) {
		// Create server item and add it to the list
		GameObject item = Instantiate(serverItemPrefab, Vector3.zero, Quaternion.identity) as GameObject;
		item.transform.SetParent(serverList.transform, false);
		item.GetComponent<ServerButton>().Setup(host);
	}

	public void HostGame() {
		GameManager.instance.HostGame();
	}

	public void RefreshList() {
		// Clear server list
		int serverItemLen = serverList.transform.childCount;
		for (int i = 0; i < serverItemLen; i++) {
			Destroy(serverList.transform.GetChild(i).gameObject);
		}

		// Ask for a new list of hosts
		MasterServer.ClearHostList();
		MasterServer.RequestHostList(GameManager.UnityGameType);
		refreshing = true;
	}

	public void ChangedNick(string nick) {
		PlayerPrefs.SetString("Nickname", nick);
	}
}
