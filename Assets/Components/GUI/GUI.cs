﻿using UnityEngine;
using UnityEngine.UI;

public class GUI : MonoBehaviour {
	public ScrollRect eventPanel, chatPanel;
	public GameObject eventContainer, chatContainer;
	public GameObject eventPrefab, chatPrefab;

	public void AddEvent(string text) {
		GameObject eventItem = Instantiate(eventPrefab, Vector3.zero, Quaternion.identity) as GameObject;
		eventItem.GetComponent<ListItem>().SetText(text);
		eventItem.transform.SetParent(eventContainer.transform, false);
		eventPanel.verticalNormalizedPosition = 1;
	}

}
