﻿using UnityEngine;

public abstract class Interpolator<T> {
	public bool stopped { get; protected set; }
	public float time = 0;
	public float interpolationTime = 1.0f / Network.sendRate;

	public T Delta { get; protected set; }
	public T Value { get { return Get(); } }

	public abstract bool Start(T v);
	protected abstract T Get();

	protected void Update() {
		if (stopped) return;
		time += Time.deltaTime;
		if (time > interpolationTime) {
			stopped = true;
		}
	}
}

public class VectorInterpolator : Interpolator<Vector3> {
	const float min = 0.01f;

	public override bool Start(Vector3 v) {
		time = 0;
		Delta = v;
		return stopped = Mathf.Abs(v.x) < min && Mathf.Abs(v.y) < min && Mathf.Abs(v.z) < min;
	}

	protected override Vector3 Get() {
		Update();
		if (stopped) return Vector3.zero;
		return Delta * Time.deltaTime / interpolationTime;
	}
}