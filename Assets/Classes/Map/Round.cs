﻿using UnityEngine;
public class Round {
	#region Round structures and data types
	public enum GameType {
		Deathmatch
	}

	public enum RoundStatus {
		PreMatch,
		InProgress,
		AfterMatch
	}
	#endregion

	#region Variables
	public float duration = 0;
	public float start = 0;
	public GameType type {
		get { return _type; }
		set {
			switch (value) {
				case GameType.Deathmatch:
					mode = new Deathmatch();
					break;
			}
			_type = value; 
		}
	}
	public RoundStatus status { get; private set; }

	GameType _type;
	GameMode mode;
	float prematchPeriod = 5;
	float aftermatchPeriod = 10;
	#endregion

	#region Public methods

	public void StartPreMatch() {
		GameManager.instance.BroadcastEvent("Match starting in " + prematchPeriod + " seconds..");
		start = Time.time;
		status = RoundStatus.PreMatch;
	}

	public void StartMatch() {
		GameManager.instance.BroadcastEvent("Starting match..");
		start = Time.time;
		status = RoundStatus.InProgress;

		// Create all players and start the match afterwards
		NetworkSync.CreateSync("CreatePlayer");
		MapScript.currentInstance.netView.RPC("CreatePlayerRPC", RPCMode.All);
		NetworkSync.AfterSync("CreatePlayer", () => {
			mode.StartMatch();
		});
	}

	public void EndMatch() {
		GameManager.instance.BroadcastEvent("Time's up!");
		start = Time.time;
		mode.EndMatch();
		status = RoundStatus.AfterMatch;
	}

	public void ForceEnd() {
		start = Time.time;
		EndMatch();
	}

	public void Update() {
		switch (status) {
			case RoundStatus.PreMatch:
				if (Time.time - start > prematchPeriod) {
					StartMatch();
				}
				break;
			case RoundStatus.InProgress:
				if (Time.time - start > duration) {
					EndMatch();
				}
				break;
			case RoundStatus.AfterMatch:
				if (Time.time - start > aftermatchPeriod) {
					StartPreMatch();
				}
				break;
		}
	}

	#endregion
}
