﻿public abstract class GameMode {
	public virtual void StartMatch() { }
	public virtual void EndMatch() { }
}
