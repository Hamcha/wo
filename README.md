# **W**ait's **O**ver
**A WFH-inspired game from people who care**

------------------------------

### Goals

- Make a fun arena TPS
- Be as open source as possible. No closed third party plugins or anything that has weird EULAs (ie. SuperSplines, NGUI)

### Current status

Nothing works, basically.  
Still working on getting even basic movement sorted out. Will take a while.

### Coding guidelines in a flash

- No Unityscript or Boo, just C#
- Avoid abusing `var`, use full types, you are not Orihaus (unless you are, in that case.. Hey there!)
    - If it's some weird iterator type or something like that, don't bother and just go with `var`.
- Try to keep the coding style you find, smart tabs where you can, and all that.